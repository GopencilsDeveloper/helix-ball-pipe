﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoBehaviour
{
    #region Editor Params
    #endregion

    #region Params
    private static DataManager _instance;
    private int currentLevel;
    public int score;
    #endregion

    #region CONST
    public static string LEVEL = "Level";
    public static string SCORE = "Score";
    #endregion

    #region Properties
    //Singleton
    public static DataManager Instance { get { return _instance ?? GameObject.FindObjectOfType<DataManager>(); } }

    public int CurrentLevel { get { return currentLevel; } set { currentLevel = value; } }

    public int Score { get { return score; } set { score = value; } }
    #endregion

    #region Events
    #endregion

    #region Methods

    public void ResetAll()
    {
        PlayerPrefs.DeleteAll();;
    }

    private void Awake()
    {
        // ResetAll();
        LoadData();
    }

    public void LevelUp()
    {
        currentLevel++;
    }

    public void AddScore(int score)
    {
        this.score += score;
    }

    public void SaveData(int level, int score)
    {
        PlayerPrefs.SetInt(SCORE, score);
        PlayerPrefs.SetInt(LEVEL, level);
    }

    public void LoadData()
    {
        currentLevel = PlayerPrefs.GetInt(LEVEL);
        score = PlayerPrefs.GetInt(SCORE);
    }

    #endregion
}
