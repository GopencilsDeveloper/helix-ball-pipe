﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum GameState
{
    Menu,
    Ingame,
    Endgame,
}

public class GameManager : MonoBehaviour
{
    #region Editor Params
    public GameObject ball;
    public Spawner spawner;
    public CylinderController cyl;
    #endregion

    #region Params
    private static GameManager _instance;
    [SerializeField]
    private GameState currentState;
    #endregion

    #region Properties
    //Singleton
    public static GameManager Instance { get { return _instance ?? GameObject.FindObjectOfType<GameManager>(); } }

    public GameState CurrentState { get; private set; }
    #endregion

    #region Events
    public event System.Action<GameState> OnStateChange;
    #endregion

    #region Methods
    public void SetGameState(GameState state)
    {
        CurrentState = state;
        OnStateChange(state);
    }

    private void Start()
    {
        Application.targetFrameRate = 60;
        SetGameState(GameState.Menu);
        cyl.gameObject.SetActive(true);
        cyl.Init();
        spawner.SpawnSpiral(Random.value <= 0.5f ? -1 : 1);

        ball.SetActive(true);
        ThemeManager.Instance.SetRandomTheme();
    }

    public void PlayGame()
    {
        SetGameState(GameState.Ingame);
        // spawner.listNormalStairs.Clear();
        GameAnalyticsManager.Instance.Log_StartGame();
        FacebookAnalytics.Instance.LogGame_startEvent(1);
    }

    public void EndGame()
    {
        SetGameState(GameState.Endgame);
        GameAnalyticsManager.Instance.Log_EndGame();
        FacebookAnalytics.Instance.LogGame_end_levelsEvent(1);
    }

    public void ReplayGame()
    {
        SceneManager.LoadScene("Game");
    }

    public void OnApplicationQuit()
    {
        GameManager._instance = null;
    }
    #endregion
}
