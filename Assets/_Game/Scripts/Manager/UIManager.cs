﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class UIManager : MonoBehaviour
{
    #region Editor Params
    public GameObject Menu;
    public GameObject InGame;
    public GameObject EndGame;

    public CylinderController cylinderController;
    public Slider sliderProgress;
    public Transform ball;
    public Animator levelUp;
    public Text scoreTxt;
    public TextMeshProUGUI scoreTMPEndgame;
    public Text levelTxt;
    public TextMeshProUGUI levelTMPEndGame;
    #endregion

    #region Params
    private static UIManager _instance;
    private GameManager GM;
    #endregion

    #region Properties
    //Singleton
    public static UIManager Instance { get { return _instance ?? GameObject.FindObjectOfType<UIManager>(); } }
    #endregion

    #region Events
    #endregion

    #region Methods
    private void Awake()
    {
        GM = GameManager.Instance;
        RegisterEvents();
    }

    private void RegisterEvents()
    {
        GM.OnStateChange += HandleOnStateChange;
    }

    private void HandleOnStateChange(GameState gameState)
    {
        switch (gameState)
        {
            case GameState.Menu:
                ShowMenu();
                ShowInGame();
                HideEnd();
                break;
            case GameState.Ingame:
                ShowInGame();
                HideMenu();
                HideEnd();
                break;
            case GameState.Endgame:
                ShowEnd();
                HideMenu();
                HideInGame();
                break;
            default:
                break;
        }
    }

    public void ShowMenu()
    {
        Menu.SetActive(true);
    }
    public void ShowInGame()
    {
        InGame.SetActive(true);
        UpdateScore();
        UpdateLevel();
    }
    public void ShowEnd()
    {
        EndGame.SetActive(true);
        UpdateScore();
        UpdateLevel();
    }

    public void HideMenu()
    {
        Menu.SetActive(false);
    }
    public void HideInGame()
    {
        InGame.SetActive(false);
    }
    public void HideEnd()
    {
        EndGame.SetActive(false);
    }

    public void ShowLevelUp()
    {
        levelUp.gameObject.SetActive(true);
        levelUp.Play("LevelUp_Show");
    }

    public void HideLevelUp()
    {
        levelUp.gameObject.SetActive(false);
    }

    void UpdateSlider()
    {
        sliderProgress.value = (float)((ball.position.y + 15f) / cylinderController.TopHeight.y);
    }

    public void UpdateScore()
    {
        scoreTxt.DOText(DataManager.Instance.Score.ToString(), 0.5f, true, ScrambleMode.All);
        scoreTMPEndgame.text = "SCORE\n" + DataManager.Instance.Score.ToString();
    }

    public void UpdateLevel()
    {
        levelTxt.text = "LEVEL " + (DataManager.Instance.CurrentLevel).ToString();
        levelTMPEndGame.text = "LEVEL\n" + (DataManager.Instance.CurrentLevel).ToString();
    }

    void Update()
    {
        if (GM.CurrentState == GameState.Ingame)
        {
            UpdateSlider();
        }
    }

    #endregion
}
