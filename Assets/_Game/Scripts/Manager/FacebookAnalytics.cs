﻿using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using UnityEngine;
public class FacebookAnalytics : MonoBehaviour
{

    private static FacebookAnalytics instance;
    public static FacebookAnalytics Instance
    {
        get
        {
            return instance != null ? instance : GameObject.FindObjectOfType<FacebookAnalytics>();
        }
    }

    void Awake()
    {
        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            // Already initialized, signal an app activation App Event
            FB.ActivateApp();
        }
    }
    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            // Signal an app activation App Event
            FB.ActivateApp();
            // Continue with Facebook SDK
            // ...
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }
    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }   // Unity will call OnApplicationPause(false) when an app is resumed
        // from the background
    void OnApplicationPause(bool pauseStatus)
    {
        // Check the pauseStatus to see if we are in the foreground
        // or background
        if (!pauseStatus)
        {
            //app resume
            if (FB.IsInitialized)
            {
                FB.ActivateApp();
            }
            else
            {
                //Handle FB.Init
                FB.Init(() =>
                {
                    FB.ActivateApp();
                });
            }
        }
    }
    public void LogGame_startEvent(double valToSum)
    {
        FB.LogAppEvent(
            "game_start",
            (float)valToSum
        );
    }
    public void LogGame_endEvent(double valToSum)
    {
        FB.LogAppEvent(
            "game_end",
            (float)valToSum
        );
    }
    public void LogGame_end_endlessEvent(double valToSum)
    {
        FB.LogAppEvent(
            "game_end_endless",
            (float)valToSum
        );
    }
    public void LogGame_end_levelsEvent(double valToSum)
    {
        FB.LogAppEvent(
            "game_end_levels",
            (float)valToSum
        );
    }
    public void LogReward_adsEvent(double valToSum)
    {
        FB.LogAppEvent(
            "reward_ads",
            (float)valToSum
        );
    }
    public void LogInterstitial_adsEvent(double valToSum)
    {
        FB.LogAppEvent(
            "interstitial_ads",
            (float)valToSum
        );
    }
}