﻿using GameAnalyticsSDK;
using UnityEngine;
public class GameAnalyticsManager : MonoBehaviour
{
    private static GameAnalyticsManager instance; public static GameAnalyticsManager Instance
    {
        get
        {
            return instance != null ? instance : GameObject.FindObjectOfType<GameAnalyticsManager>();
        }
    }
    private void Awake()
    {
        GameAnalytics.Initialize();
    }
    public void Log_StartGame(string progression = "game")
    {
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, progression);
    }
    public void Log_EndGame(string progression = "game")
    {
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, progression, DataManager.Instance.CurrentLevel);
    }
}