﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct Theme
{
    public int index;
    public Color cyl;
    public Color cylBase;
    public Color top;
    public Color bot;
    public Color black;
    public Color white;
}

public class ThemeManager : MonoBehaviour
{
    #region Editor Params
    public Theme[] themes;

    [Header("References")]
    public Image sliderBG;
    public Image sliderFill;
    public Image sliderHandle;

    public Text txtLevel;
    public Outline outlineLevel;
    public Text txtScore;
    public Outline outlineScore;
    public Text txtGameName;
    public Outline outlineGameName;
    public Text txtTut;
    public Outline outlineTut;

    #endregion

    #region Params
    private static ThemeManager _instance;

    public static ThemeManager Instance { get { return _instance ?? GameObject.FindObjectOfType<ThemeManager>(); } }
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods

    public void SetRandomTheme()
    {
        Theme theme = themes[Random.Range(0, themes.Length)];
        CylinderController.Instance.SetColor(theme.cyl, theme.cylBase);
        BGController.Instance.SetColor(theme.top, theme.bot);

        if (theme.index == 0) //BW
        {
            txtLevel.color =
            txtScore.color =
            txtGameName.color =
            txtTut.color =
            sliderBG.color =
            theme.white;

            outlineLevel.effectColor =
            outlineScore.effectColor =
            outlineGameName.effectColor =
            outlineTut.effectColor =
            sliderFill.color =
            sliderHandle.color =
            theme.black;
        }
        else
        if (theme.index == 1) //WB
        {
            txtLevel.color =
            txtScore.color =
            txtGameName.color =
            txtTut.color =
            sliderBG.color =
            theme.black;

            outlineLevel.effectColor =
            outlineScore.effectColor =
            outlineGameName.effectColor =
            outlineTut.effectColor =
            sliderFill.color =
            sliderHandle.color =
            theme.white;
        }
    }
    #endregion
}
