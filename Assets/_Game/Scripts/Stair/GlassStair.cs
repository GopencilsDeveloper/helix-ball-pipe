﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathologicalGames;

public class GlassStair : Stair
{
    private void OnCollisionExit(Collision other)
    {
        if (other.gameObject.tag == "Ball")
        {
            Invoke("Inactive", 1f);
        }
    }

    void Inactive()
    {
        PoolManager.Pools["Manager"].Despawn(transform);
    }
}
