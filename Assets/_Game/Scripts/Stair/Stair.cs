﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StairType
{
    Normal,
    Jump,
    Glass,
    Invi,
    Red
}

public class Stair : MonoBehaviour
{
    #region Editor Params
    #endregion

    #region Params
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods

    public void SetSize()
    {

    }

    public virtual void SetType(StairType type) { }

    public void SetPosition(float postionY, float rotationY)
    {
        transform.localPosition = new Vector3(0f, postionY, 0f);
        transform.localRotation = Quaternion.Euler(0f, rotationY, 0f);
    }
    #endregion
}
