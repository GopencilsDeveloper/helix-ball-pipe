﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalStair : Stair
{
    public Color blue;
    public Color yellow;
    public Color red;
    public StairType type;
    public MeshRenderer meshRenderer;
    public GameObject[] coins;
    public Collider[] coinsCollider;
    public int colorInt;
    public int index;

    private void Start()
    {
        if (Random.value < 0.2f)
        {
            ActiveCoin(Random.Range(1, coins.Length + 1));
        }
    }
    
    public void SetColor(int index)
    {
        this.index = index;
        if (index > 2)
        {
            index = 0;
        }
        else
        if (index == 0)
        {
            meshRenderer.material.color = blue;
            colorInt = 0;
        }
        else
        if (index == 1)
        {
            meshRenderer.material.color = yellow;
            colorInt = 1;
        }
        else
        if (index == 2)
        {
            meshRenderer.material.color = red;
            colorInt = 2;
        }

    }

    public void ActiveCoin(int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            coins[i].SetActive(true);
            coinsCollider[i].enabled = true;
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Ball")
        {
            if (this.colorInt != other.gameObject.GetComponent<BallController>().ColorIndex)
            {
                other.gameObject.SetActive(false);
                GameManager.Instance.EndGame();
            }
        }
    }
}
