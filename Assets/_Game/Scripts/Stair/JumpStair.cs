﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class JumpStair : Stair
{
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Ball")
        {
            other.gameObject.GetComponent<BallController>().Boost();
            Camera.main.gameObject.GetComponent<CameraController>().CanFollow = true;
            Camera.main.gameObject.GetComponent<CameraController>().proCamera.enabled = false;
        }
    }

}