﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedStair : Stair
{
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Ball")
        {
            other.gameObject.SetActive(false);
        }
    }
}
