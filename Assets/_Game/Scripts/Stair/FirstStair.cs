﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstStair : Stair
{
    public MeshRenderer meshRenderer;
    private BallController ball;
    private void Start()
    {
        ball = GameObject.FindGameObjectWithTag("Ball").GetComponent<BallController>();

        meshRenderer.material.color = ball.meshRenderer.material.color;
    }


}
