﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ProceduralToolkit.Examples.Primitives;
using UnityEngine.EventSystems;

public class CylinderController : MonoBehaviour
{
    #region Editor Params
    public MeshRenderer cylBase;
    public MeshRenderer meshRenderer;
    public Prism prism;
    [SerializeField]
    private float rotationDuration;
    public GameObject nextLevelRing;
    #endregion

    #region Params
    private static CylinderController _instance;
    private float segmentAngle;
    private bool isRotating = false;

    private Vector3 topHeight;
    private int rotationCount;

    #endregion

    #region Properties
    public float SegmentAngle { get { return segmentAngle; } set { segmentAngle = value; } }

    public Vector3 TopHeight { get { return topHeight; } set { topHeight = value; } }

    public static CylinderController Instance { get { return _instance ?? GameObject.FindObjectOfType<CylinderController>(); } }
    #endregion

    #region Events
    #endregion

    #region Methods

    public void Init()
    {
        segmentAngle = 360f / prism.segments;
        prism.height = 100 + DataManager.Instance.CurrentLevel * 5;
        topHeight = new Vector3(0f, (int)prism.height / 4f, 0f);
        nextLevelRing.transform.localPosition = topHeight;
    }

    private void Update()
    {
        if (GameManager.Instance.CurrentState == GameState.Ingame)
        {
            if (Input.GetMouseButton(0))
            {
                if (Input.mousePosition.x <= Screen.width / 2f) // Tap Left
                {
                    TurnLeft();
                }
                else // Tap Right
                {
                    TurnRight();
                }
            }
            if (Input.GetMouseButtonUp(0))
            {
                if (rotationCount >= 4) //Move over 4 step --> bonus scores
                {
                    Spawner.Instance.SpawnScoreTmp(rotationCount);
                }
                rotationCount = 0;
            }

        }
    }

    public void TurnLeft()
    {
        if (!isRotating)
        {
            StartCoroutine(C_DoRotate(-segmentAngle, rotationDuration));
        }
    }

    public void TurnRight()
    {
        if (!isRotating)
        {
            StartCoroutine(C_DoRotate(segmentAngle, rotationDuration));
        }
    }

    IEnumerator C_DoRotate(float angle, float duration)
    {
        isRotating = true;
        rotationCount++;
        Quaternion start = transform.rotation;
        Quaternion end = start * Quaternion.Euler(new Vector3(0f, angle, 0f));
        float t = 0f;
        while (t < 1f)
        {
            t += Time.deltaTime / duration;
            transform.rotation = Quaternion.Slerp(start, end, t);
            yield return null;
        }
        isRotating = false;
    }

    public void SetColor(Color colorCyl, Color colorBase)
    {
        meshRenderer.material.color = colorCyl;
        cylBase.material.color = colorBase;
    }

    #endregion
}
