﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Round : MonoBehaviour
{

    public Transform ball;
    public SpriteRenderer sprite;

    public void SetColor(Color color)
    {
        sprite.color = new Color(color.r, color.g, color.b, 0.5f);
    }

    private void Update()
    {
        transform.position = Vector3.Lerp(transform.position, ball.position, 25 * Time.deltaTime);
    }
}
