﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Ring : MonoBehaviour
{
    #region Editor Params
    public MeshRenderer meshRenderer;
    public Color blue;
    public Color yellow;
    public Color red;
    #endregion

    #region Params
    private int colorIndex;
    #endregion

    #region Properties
    public int ColorIndex { get { return colorIndex; } set { colorIndex = value; } }
    #endregion

    #region Events
    #endregion

    #region Methods

    public void SetPosition()
    {
        transform.localPosition = new Vector3(2f, 2.5f, 0f);
    }
    public void SetRotation()
    {
        transform.localRotation = new Quaternion(0f, 90f, 0f, 0f);
    }

    public void SetColor(int index)
    {
        float rand = Random.value;
        if (index == 0)
        {
            meshRenderer.material.color = blue;
            colorIndex = 0;
        }
        else
        if (index == 1)
        {
            meshRenderer.material.color = yellow;
            colorIndex = 1;
        }
        else
        if (index == 2)
        {
            meshRenderer.material.color = red;
            colorIndex = 2;
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Ball")
        {
            BallController ball = other.gameObject.GetComponent<BallController>();
            ball.meshRenderer.material.color = meshRenderer.material.color;
            ball.ColorIndex = colorIndex;

            StartCoroutine(C_Fade());
        }
    }

    IEnumerator C_Fade()
    {
        transform.DOScale(new Vector3(1.5f, 1.5f, 1.5f), 0.5f);
        meshRenderer.material.DOFade(0f, 0.5f);
        yield return new WaitForSeconds(0.5f);
        gameObject.SetActive(false);
    }

    #endregion
}
