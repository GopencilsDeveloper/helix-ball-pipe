﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextLevelRing : MonoBehaviour
{
    #region Editor Params
    #endregion

    #region Params
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Ball")
        {
            other.GetComponent<Rigidbody>().velocity = Vector3.zero;
            other.GetComponent<Rigidbody>().useGravity = false;
            other.GetComponent<BallController>().Explode();
        }
    }
    #endregion
}
