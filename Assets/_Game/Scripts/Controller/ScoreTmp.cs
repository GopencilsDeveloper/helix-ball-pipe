﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreTmp : MonoBehaviour
{
    public Text txt;
    public void SetScore(int score)
    {
        txt.text = "+" + score.ToString();
    }

}
