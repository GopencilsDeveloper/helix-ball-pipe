﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGController : MonoBehaviour
{
    public MeshRenderer meshRenderer;

    private static BGController _instance;

    public static BGController Instance { get { return _instance ?? GameObject.FindObjectOfType<BGController>(); } }

    public void SetColor(Color top, Color bot)
    {
        meshRenderer.material.SetColor("_Color", top);
        meshRenderer.material.SetColor("_Color2", bot);
    }
}
