﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathologicalGames;
using System.Linq;

public class Spawner : MonoBehaviour
{
    #region Editor Params
    [Header("Prefabs")]
    public GameObject normalStairPrefab;
    public GameObject jumpStairPrefab;
    public GameObject inviStairPrefab;
    public GameObject scoreTMPPrefab;
    public GameObject ringPrefab;

    [Header("Others")]
    public CylinderController cylinderController;
    public Transform baseTrans;
    public Transform targetPos;
    #endregion

    #region Params
    private static Spawner _instance;
    private int index = 0;
    private int firstColor;
    private int changeColorStep = 2;
    private float heightBetweenStairs = 0.225f;
    private float inviPercent = 0.02f;
    public List<NormalStair> listNormalStairs = new List<NormalStair>();
    private List<int> colorTypes = new List<int>() { 0, 1, 2 };
    private int[] colorsSelected = new int[2];
    #endregion

    #region Properties
    public static Spawner Instance { get { return _instance ?? GameObject.FindObjectOfType<Spawner>(); } }

    #endregion

    #region Events
    #endregion

    #region Methods

    private int signRing = 1;
    private int stepRing;
    public void SpawnSpiral(int sign)
    {
        IListExtensions.Shuffle(colorTypes);

        float startHeight = baseTrans.localPosition.y + 1f;
        float stepAngle = cylinderController.SegmentAngle;
        int stairAmount = (int)(cylinderController.prism.segments * 2f + DataManager.Instance.CurrentLevel * 3);
        stepRing = 15 - (int)(DataManager.Instance.CurrentLevel / 5f);
        stepRing = Mathf.Clamp(stepRing, 5, 15);

        int inviAmount = (int)(DataManager.Instance.CurrentLevel / 5f) + 1;
        inviAmount = Mathf.Clamp(inviAmount, 3, 20);
        int[] invis = RandomIndexes(inviAmount, stairAmount);

        for (int i = 0; i < stairAmount; i++) //Spawn Normal Stairs
        {
            Transform stair = SpawnStair(normalStairPrefab, startHeight + i * heightBetweenStairs, sign * i * stepAngle, cylinderController.transform);
            NormalStair normalStair = stair.GetComponent<NormalStair>();
            listNormalStairs.Add(normalStair);

            if (i % 3 == 0)
            {
                normalStair.SetColor(colorTypes[0]);
            }
            else
            {
                normalStair.SetColor(colorTypes[Random.value < 0.3f ? 0 : 1]);
            }

            if (i > 0 && i % stepRing == 0)
            {
                Ring ring = SpawnRing(listNormalStairs[i].transform).GetComponent<Ring>();
                signRing *= -1;
                if (signRing == 1)
                {
                    ring.SetColor(colorTypes[0]);
                }
                else
                if (signRing == -1)
                {
                    ring.SetColor(colorTypes[1]);
                }
            }
        }

        for (int j = 0; j < invis.Length; j++)
        {
            listNormalStairs[invis[j]].gameObject.SetActive(false);

            if (Random.value < 0.4f)
            {
                listNormalStairs[invis[j] + 1].gameObject.SetActive(false);
            }
        }

        SpawnStair(jumpStairPrefab, startHeight + stairAmount * heightBetweenStairs, sign * stairAmount * stepAngle, cylinderController.transform);

        BallController.Instance.SetColor(listNormalStairs[0].index);
    }

    int[] RandomIndexes(int amount, int range)
    {
        int[] temp = new int[amount];
        for (int i = 0; i < amount; i++)
        {
            int rnd = Random.Range(5, range - 3);
            temp[i] = rnd;
        }
        return temp;
    }

    public void SpawnScoreTmp(int score)
    {
        DataManager.Instance.AddScore(score);
        UIManager.Instance.UpdateScore();

        Transform instance = PoolManager.Pools["Manager"].Spawn(scoreTMPPrefab, targetPos);
        ScoreTmp scoreTmp = instance.GetComponent<ScoreTmp>();
        scoreTmp.SetScore(score);
        StartCoroutine(C_Despawn(instance, 0.5f));
    }

    IEnumerator C_Despawn(Transform instance, float duration)
    {
        yield return new WaitForSeconds(duration);
        PoolManager.Pools["Manager"].Despawn(instance);
    }


    Transform SpawnStair(GameObject prefab, float posY, float rotY, Transform parents)
    {
        Transform instance = PoolManager.Pools["Manager"].Spawn(prefab, parents);
        Stair stair = instance.GetComponent<Stair>();
        stair.SetPosition(posY, rotY);
        return instance;
    }

    Transform SpawnRing(Transform parents)
    {
        Transform instance = PoolManager.Pools["Manager"].Spawn(ringPrefab, parents);
        Ring ring = instance.GetComponent<Ring>();
        ring.SetPosition();
        ring.SetRotation();
        return instance;
    }

    #endregion
}
