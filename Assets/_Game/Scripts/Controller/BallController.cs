﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using MoreMountains.NiceVibrations;

public class BallController : MonoBehaviour
{
    #region Editor Params
    public ParticleSystem trail;
    public ParticleSystem confetti;
    public MeshRenderer meshRenderer;
    public Color blue;
    public Color yellow;
    public Color red;
    public Round round;
    #endregion

    #region Params
    private Rigidbody rgbd;
    private int colorIndex;
    DataManager data;
    private static BallController _instance;
    #endregion

    #region Properties
    public int ColorIndex { get { return colorIndex; } set { colorIndex = value; } }

    public static BallController Instance { get { return _instance ?? GameObject.FindObjectOfType<BallController>(); } }
    #endregion

    #region Events
    #endregion

    #region Methods
    private void Start()
    {
        Initialize();
        data = DataManager.Instance;
    }

    void Initialize()
    {
        rgbd = GetComponent<Rigidbody>();
    }

    public void SetColor(int index = 0)
    {
        if (index == 0)
        {
            meshRenderer.material.color = blue;
            colorIndex = 0;
        }
        else
            if (index == 1)
        {
            meshRenderer.material.color = yellow;
            colorIndex = 1;
        }
        else
            if (index == 2)
        {
            meshRenderer.material.color = red;
            colorIndex = 2;
        }
        round.gameObject.SetActive(true);
        round.SetColor(meshRenderer.material.color);
    }

    private void OnCollisionEnter(Collision other) //Bouncing
    {
        if (other.gameObject.tag == "Stair")
        {
            rgbd.velocity = Vector3.zero;
            rgbd.AddForce(0f, 465f, 0f);
            MMVibrationManager.VibrateLight();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Ring")
        {
            MMVibrationManager.VibrateLight();
            Spawner.Instance.SpawnScoreTmp(3);
            round.SetColor(meshRenderer.material.color);
        }
        if (other.gameObject.tag == "Coin")
        {
            MMVibrationManager.VibrateLight();
            Spawner.Instance.SpawnScoreTmp(1);
        }
    }

    bool isRotating = false;

    IEnumerator C_DoRandomRotate(float duration)
    {
        isRotating = true;
        Quaternion start = transform.rotation;
        Quaternion end = Random.rotation;
        float t = 0f;
        while (t < 1f)
        {
            t += Time.deltaTime / duration;
            transform.rotation = Quaternion.Slerp(start, end, t);
            yield return null;
        }
        isRotating = false;
    }

    public void Explode()
    {
        StartCoroutine(C_Explode());
    }

    IEnumerator C_Explode()
    {
        data.LevelUp();
        data.SaveData(data.CurrentLevel, data.Score);

        Vector3 tempScale = transform.localScale;
        float t = 0f;
        while (t < 1)
        {
            t += Time.deltaTime / 0.1f;
            transform.localScale = Vector3.Lerp(tempScale, Vector3.zero, t);
            yield return null;
        }
        yield return new WaitForEndOfFrame();

        confetti.gameObject.SetActive(true);
        confetti.transform.position = transform.position;
        confetti.Stop();
        confetti.Play();
        yield return new WaitForSeconds(0.2f);
        UIManager.Instance.ShowLevelUp();
        yield return new WaitForSeconds(1f);
        UIManager.Instance.HideLevelUp();
        yield return new WaitForSeconds(0.5f);
        GameManager.Instance.EndGame();
    }

    public void Boost()
    {
        StartCoroutine(C_Boost());
    }
    IEnumerator C_Boost()
    {
        round.gameObject.SetActive(false);
        transform.DOMove(new Vector3(0f, CylinderController.Instance.nextLevelRing.transform.position.y + 1f, -2f), 0.75f);
        CylinderController.Instance.meshRenderer.material.DOColor(Color.black, 2f);
        BGController.Instance.meshRenderer.material.DOColor(Color.black, 2f);
        meshRenderer.material.color = Color.red;
        trail.gameObject.SetActive(true);
        trail.Stop();
        trail.Play();
        yield return new WaitForSeconds(0.75f);
        trail.gameObject.SetActive(false);
    }

    #endregion

}
