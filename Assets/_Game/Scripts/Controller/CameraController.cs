﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Com.LuisPedroFonseca.ProCamera2D;

public class CameraController : MonoBehaviour
{
    public BallController ball;
    public ProCamera2D proCamera;

    private Vector3 offset;
    private Vector3 startPos;
    private float tempFOV;
    private bool canFollow;

    public bool CanFollow
    {
        get
        {
            return canFollow;
        }

        set
        {
            canFollow = value;
        }
    }

    void Start()
    {
        offset = transform.position - ball.transform.position;
        startPos = transform.position;

        tempFOV = Camera.main.fieldOfView;
        canFollow = false;
    }

    void LateUpdate()
    {
        if (canFollow)
        {
            transform.position = Vector3.Lerp(startPos, ball.transform.position + offset, 1f);
        }
    }

    public void Zoom()
    {
        StartCoroutine(C_Zoom());
    }

    IEnumerator C_Zoom()
    {
        float t = 0f;
        while (t < 1f)
        {
            t += Time.deltaTime / 1f;
            Camera.main.fieldOfView = Mathf.Lerp(tempFOV, 45, t);
            // yield return new WaitForSeconds(0.5f);
            // t = 0;
            // tempFOV = Camera.main.fieldOfView;
            // t += Time.deltaTime / 0.5f;
            // Camera.main.fieldOfView = Mathf.Lerp(tempFOV, 60, t);
            yield return null;
        }
    }

}
