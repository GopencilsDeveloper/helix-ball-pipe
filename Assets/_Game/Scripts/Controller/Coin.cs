﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    #region Editor Params
    public GameObject coin;
    public ParticleSystem coinPS;
    #endregion

    #region Params
    private Quaternion tempRotation;
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods

    public void AutoRotate()
    {
        transform.Rotate(0f, 100f * Time.deltaTime, 0f);
    }

    private void Update()
    {
        AutoRotate();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Ball")
        {
            coin.SetActive(false);
            coinPS.gameObject.SetActive(true);
            coinPS.Stop();
            coinPS.Play();
            Invoke("Disable", 0.3f);
        }
    }

    void Disable()
    {
        gameObject.SetActive(false);
    }

    #endregion
}
